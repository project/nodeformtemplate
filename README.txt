
This module allows you to define a template for each content type.

Once activated, a new section called "Form template" will be available under the
content type administration area, eg for Story it will be:

Home > Administer > Content management > Content types > Story

There you will find a replica of the content type form, where you will be able
to fill textfields with default content. When creating new stories the form
will be populated with the text you previously defined.

Templates can include placeholders starting with a % sign, which will be
replaced with the query string parameter having the same name: useful to build
bookmarklets.

